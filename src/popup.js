const inputElement = document.getElementById("gitlab_url");
inputElement.value = localStorage.getItem("gitlabUrl");

const sendMessage = (value) =>
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    chrome.tabs.executeScript(tabs[0].id, { file: "index.js" }, () => {
      chrome.tabs.sendMessage(tabs[0].id, { gitlabUrl: value });
    });
  });

sendMessage(inputElement.value);

inputElement.addEventListener("change", async (event) => {
  localStorage.setItem("gitlabUrl", event.target.value);
  await sendMessage(event.target.value);
});
